import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import home from '../imports/components/home/home';
import subscribedItemsSidebar from '../imports/components/subscribedItemsSidebar/subscribedItemsSidebar';
import classItem from '../imports/components/classItem/classItem';
import topic from '../imports/components/topic/topic';
import login from '../imports/components/login/login';
import resourceList from '../imports/components/resource/resourceList';
import rating from '../imports/components/rating/rating';
import comment from '../imports/components/comment/comment';
import flag from '../imports/components/flag/flag';
import modal from '../imports/components/modal/modal';
angular.module('elsewhere', [
  angularMeteor
    , home.name
    , subscribedItemsSidebar.name
    , classItem.name
    , topic.name
    , login.name
    , resourceList.name
    , rating.name
    , comment.name
    , flag.name
    , modal.name
    , comment.name
    , flag.name
    , uiRouter
]).config(config);

function config($locationProvider, $urlRouterProvider) {
    'ngInject';
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/login');
}
$.getScript("https://apis.google.com/js/platform.js", function (data, textResult, jqXHR) {
    window.signOut = signOut;
});

function signOut() {
    // this function needs to be here so that it persists throughout the application
    //google's signout method.  see: https://developers.google.com/identity/sign-in/web/sign-in
    //console.log(textResult);
    console.log(gapi);
    //console.log(window.gapi);
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}