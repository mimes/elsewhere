import { Meteor } from 'meteor/meteor';
import '../imports/api/Class.js';
import '../imports/api/SubscribedClass.js';
import '../imports/api/Topic.js';
import '../imports/api/User.js';
import '../imports/api/Resource.js';
import '../imports/api/Comment.js';
import '../imports/api/Rating.js';
import '../imports/api/FlaggedItem.js';
Meteor.startup(() => {
  // code to run on server at startup
   
});
