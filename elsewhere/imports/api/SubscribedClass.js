import { Mongo } from 'meteor/mongo';

SubscribedClassSchema = new SimpleSchema({
    classId: {
        type: String
    }
    , subscribedClassId: {
        type: String
        , regEx: SimpleSchema.RegEx.Id
        , optional: false
    },
    className:{
     type:String,
        optional:false
    }
    , email: {
        type: String
        , optional: false
    }
});

export const SubscribedClasses = new Mongo.Collection('SubscribedClasses');
if(Meteor.isServer){
SubscribedClasses._ensureIndex({email:1,className:1},{unique:true});
}
