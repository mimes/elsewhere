import { Mongo } from 'meteor/mongo';

ClassSchema = new SimpleSchema({
    className: {
        type: String,
        optional:false
    }
    , description: {
        type: String
        , optional: false
    },
    userid:{
        type: String,
        optional: false
    }
});

export const Classes = new Mongo.Collection('Classes');