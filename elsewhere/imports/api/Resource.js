import { Mongo } from 'meteor/mongo';

ResourceSchema = new SimpleSchema({
    topicName: {
        type: String,
        optional:false
    },
    topicId:{
        type:String,
        optional:false
    }
    , description: {
        type: String
        , optional: false
    },
    content:{
        type:String,
        optional:false
    },
    userId:{
        type:String,
        optional:false
    }
});

export const Resources = new Mongo.Collection('Resources');
