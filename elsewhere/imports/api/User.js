import {
    Mongo
}
from 'meteor/mongo';
UserSchema = new SimpleSchema({
    email: {
        type: String
        , optional: false
    }
    , userId: {
        type: String
        , regEx: SimpleSchema.RegEx.Id
        , optional: false
    }
    , name: {
        type: String
        , optional: false
    }
   
});
export const Users = new Mongo.Collection('Users');
if(Meteor.isServer){
Users._ensureIndex({email:1},{unique:true});
}