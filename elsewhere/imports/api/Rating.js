import { Mongo } from 'meteor/mongo';

RatingSchema = new SimpleSchema({
    resourceId: {
        type: String,
        optional:false
    },
    userId:{
        type:String,
        optional:false
    },
    numberOfStars:{
        type: Number,
        optional:false
    }
});

export const Ratings = new Mongo.Collection('Ratings');
if(Meteor.isServer){
Ratings._ensureIndex({userId:1},{unique:true});
    
Meteor.methods({
  getRatingData( filter ) {
   

    let group = {
      _id: {
        resourceId: '$resourceId'
      },
      total: {
        $sum: '$numberOfStars'
      }
    };


    return Ratings.aggregate(
      { $group: group }
    );
  }
});    
 
}