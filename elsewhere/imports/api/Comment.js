import { Mongo } from 'meteor/mongo';

CommentSchema = new SimpleSchema({
    resourceId: {
        type: String,
        optional:false
    },
    comment:{
        type:String,
        optional:false
    }
    , userId: {
        type: String
        , optional: false
    }
    /*
    TODO:  allow user to 
    ,
    
    content:{
        type:String,
        optional:false
    }*/
});

export const Comments = new Mongo.Collection('Comments');
