import { Mongo } from 'meteor/mongo';

TopicSchema = new SimpleSchema({
    description: {
        type: String,
        optional: false
    }
    , classId: {
        type: String,
        optional:false
    }
    , topicName: {
        type: String,
        optional: false
    },
    userId: {
        type: String,
        optional: false
    }
});

export const Topics = new Mongo.Collection('Topics');