import {
    Mongo
}
from 'meteor/mongo';
/* using an Enum for itemType:  
    0: flagged class
    1: flagged topic 
    2: flagged resource
    3: flagged comment
    */
FlaggedItemsSchema = new SimpleSchema({
    itemType: {
        type: Number
        , optional: false
    }
    , itemId: {
        type: String
        , optional: false
    }
    , userFlagged: {
        type: String
        , optional: false
    }
    , userThatFlagged: {
        type: String
        , optional: false
    }
});
export const FlaggedItems = new Mongo.Collection('FlaggedItems');
if (Meteor.isServer) {
    FlaggedItems._ensureIndex({
        itemId: 1
        , userThatFlagged: 1
    }, {
        unique: true
    });
}