import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import template from './rating.html';
import {
    Ratings
}
from '../../api/Rating.js';
import {
    Resources
}
from '../../api/Resource.js';

function RatingCtrl($scope) {
    $scope.setRating = function () {
        //sets the rating in the mongo database:  if a record exists, update it. otherwise create
        //TODO
    }
    $scope.sumRatings = function (ratings) {
        $scope.ratingSum = 0;
        $scope.ratings.forEach(function (aRatings) {
            ratingSum += aRating.numberOfStars;
        });
    }
    $scope.$onInit = function () {
        $scope.newRating = 0;
        console.log("rating init");
        $scope.myResourceId = $scope.$parent.myResourceId;
        console.log($scope.myResourceId);
    }
    
    
    
    $scope.rateResource = function () {
        var myUserId = JSON.parse(localStorage.getItem("currentUser"))._id;
        var existingRating = Ratings.find({
            userId: myUserId
        }).fetch();
        
        if ((existingRating._id) !== undefined & existingRating._id & "undefined" & existingRating._id !== null) {
            Ratings.insert({userId: myUserId, resourceId: $scope.myResourceId, numberOfStars: parseInt($scope.newRating)})
        }
        
        else {
            //a previous rating already exists
            Ratings.update({
                _id: existingRating._id
            }, {
                numberOfStars: parseInt($scope.newRating)
                , resourceId: $scope.myResourceId
            }, {
                upsert: true
            });
        }
    }

    
    $scope.helpers({
        ratings() {
                return Ratings.find({
                    topicId: $scope.myTopicId
                });
            }
            , processedRatings() {
                return 0
            }
    });
}
export default angular.module('ratings', [angularMeteor, uiRouter]).component('rating', {
    templateUrl: 'imports/components/rating/rating.html'
    , controller: RatingCtrl
});