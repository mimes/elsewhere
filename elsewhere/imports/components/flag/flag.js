import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import template from './flag.html';
import {
    FlaggedItems
}
from '../../api/FlaggedItem.js';

function FlagCtrl($scope) {
    this.$onInit = function () {
        $scope.itemId = $scope.$parent.itemId
        $scope.itemType = $scope.$parent.itemType
        $scope.myUserId = JSON.parse(localStorage.getItem("currentUser")).id;
        $scope.userFlagged = $scope.$parent.contentCreatorId;
        var existingFlag = FlaggedItems.findOne({
            userThatFlagged: $scope.myUserId
            , itemId: $scope.itemId
        });
        if (!existingFlag) {
            $scope.isFlagged = false;
        }
        else {
            $scope.isFlagged = true;
        }
        
        
    }
    $scope.flagItem = function () {
        //
        $scope.isFlagged = true;
        var myUserId = $scope.myUserId;
        var existingFlag = FlaggedItems.find({
            userThatFlagged: myUserId
            , itemId: $scope.itemId
        }).fetch();
        FlaggedItems.insert({
            userThatFlagged: myUserId
            , itemType: $scope.itemType, //  <enum defined in FlaggedItem api...represents a itemType of topic
            itemId: $scope.itemId
            , userFlagged: $scope.userFlagged
        })
    }
    $scope.unFlagItem = function () {
        $scope.isFlagged = false;
        var existingFlag = FlaggedItems.findOne({
            userThatFlagged: $scope.myUserId
            , itemId: $scope.itemId
        });
        FlaggedItems.remove({
            _id: existingFlag._id
        })
    }
    $scope.helpers({});
}
export default angular.module('flag', [angularMeteor, uiRouter]).component('flag', {
    templateUrl: 'imports/components/flag/flag.html'
    , controller: FlagCtrl
});