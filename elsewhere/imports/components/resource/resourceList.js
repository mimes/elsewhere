import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import template from './resourceList.html';
import {
    Resources
}
from '../../api/Resource.js';
import {
    Topics
}
from '../../api/Topic.js';

function ResourceListCtrl($scope) {
    //flag for showing UI to add a resource
    //alert(topicId);
    this.$onInit = function () {
        var FLAGGED_ITEM_ENUM_TYPE_RESOURCE = 2;
        $scope.itemType = FLAGGED_ITEM_ENUM_TYPE_RESOURCE;
        /*console.log($scope.myResourceId)
        console.log(myResourceId)*/
        
        $scope.myTopicId = $scope.$parent.myTopic._id
        $scope.helpers({
            resources() {
                return Resources.find({
                    topicId: $scope.myTopicId
                });
            }
        });
    }
    $scope.addingResource = false;
    $scope.newResource = {};
    //get list of topics for the current class
    $scope.topics = $scope.$parent.topics;
    $scope.currentTopicId = "";
}
export default angular.module('resourceList', [angularMeteor, uiRouter]).component('resourceList', {
    templateUrl: 'imports/components/resource/resourceList.html'
    , controller: ResourceListCtrl
})