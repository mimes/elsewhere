import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import template from './modal.html';

function ModalCtrl($scope, $stateParams) {
    this.$onInit = function () {}
    $scope.helpers({});
}
export default angular.module('modal', [angularMeteor, uiRouter]).component('modal', {
    templateUrl: 'imports/components/modal/modal.html'
    , controller: ModalCtrl
    , transclude: {
        'title': 'modalTitle',
        'body': 'modalBody'
    }
});