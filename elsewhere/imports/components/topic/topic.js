import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import template from './topic.html';
import {
    Resources
}
from '../../api/Resource.js';
import {
    Topics
}
from '../../api/Topic.js';
/*
function TopicCtrl($scope) {
    $scope.topicInfo = {};
    
    console.log($scope);
    //flag for showing UI to add a resource
    $scope.addingResource = false;
    $scope.newResource = {};
    //get list of topics for the current class
    $scope.topics = $scope.$parent.topics;
    $scope.currentTopicId = "";
    $scope.helpers({
        resources() {
            return Resources.find({
                topicId: $scope.currentTopicId
            });
        }
    });
    $scope.addResource = function (topic) {
        Resources.insert({
            topicId: topic._id
            , topicName: topic.topicName
            , content: $scope.newResource.content
            , description: $scope.newResource.description
        });
        //hide UI
        $scope.addingResource = false;
        //empty new resource
        $scope.newResource = {};
    }
}*/
export default angular.module('topic', [angularMeteor, uiRouter]).directive('topic', function () {
    return {
        restrict: 'E'
        , templateUrl: 'imports/components/topic/topic.html'
        , controller: ["$scope", function ($scope) {
            console.log(this)
            var FLAGGED_ITEM_ENUM_TYPE_TOPIC = 1;
            $scope.itemType = FLAGGED_ITEM_ENUM_TYPE_TOPIC;
            $scope.showingResources = false;
            //flag for showing UI to add a resource
            $scope.addingResource = false;
            $scope.showingNewResourceForm = false;
            $scope.isCollapsed = true;
            $scope.newResource = {};
            //get list of topics for the current class
            $scope.topics = $scope.$parent.topics;
            $scope.currentTopicId = "";
            $scope.viewTopicDetails = function () {
                //tell parent scope (classitem) to collapse all topics besides myTopic
                $scope.isCollapsed = false;
                //console.log("mytopicScope:  " + $scope.myTopic._id);
                $scope.$emit("collapseOtherTopics", $scope.myTopic._id);
                $scope.showingResources = true;
            }
            $scope.$on("collapseTopic", function (e, topicIdToExclude) {
                //console.log("collapsing all but:  " + topicIdToExclude);
                if ($scope.myTopic._id === topicIdToExclude) {
                    //don't collapse me
                }
                else {
                    $scope.isCollapsed = true;
                    $scope.showingResources = false;
                }
            })
            $scope.hideResources = function () {
                $scope.showingResources = false;
            }
            $scope.showNewResourceForm = function () {
                /*console.log($("#resourceModal").find(".modal"))*/
                $("#resourceModal").find(".modal").modal();
                console.log("showing new resource form")
                $scope.showingNewResourceForm = true;
            }
            $scope.addResource = function (topic) {
                Resources.insert({
                    topicId: topic._id
                    , topicName: topic.topicName
                    , content: $scope.newResource.content
                    , description: $scope.newResource.description
                    , userId: JSON.parse(localStorage.getItem('currentUser')).id
                });
                //hide UI
                $scope.addingResource = false;
                //empty new resource
                $scope.newResource = {};
                $scope.showingNewResourceForm = false;
            }
        }]
    }
})