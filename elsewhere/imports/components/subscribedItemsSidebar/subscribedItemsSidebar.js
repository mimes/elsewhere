import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import {
    Mongo
}
from 'meteor/mongo';
import {
    SubscribedClasses
}
from '../../api/SubscribedClass.js';
import {
    Users
}
from '../../api/User.js';
import template from './subscribedItemsSidebar.html';

function SubscribedItemsSidebarCtrl($scope) {
    //console.log(Users);
    //console.log(SubscribedClasses);
    $scope.user = JSON.parse(localStorage.getItem("currentUser"));
    if ($scope.user !== null) {
        $scope.user.id = Users.find({
            email: $scope.user.email
        });
    }
    //console.log($scope.user);
    $scope.helpers({
        subscribedClasses() {
            
                return SubscribedClasses.find({
                    email: $scope.user.email
                });
            }
        
    });

}
export default angular.module('subscribedItemSidebar', [angularMeteor, uiRouter]).component('subscribedItemsSidebar', {
    templateUrl: 'imports/components/subscribedItemsSidebar/subscribedItemsSidebar.html'
    , controller: SubscribedItemsSidebarCtrl
}).config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider.state('class', {
        url: '/class/:id'
        , template: '<class></class>'
    });
}