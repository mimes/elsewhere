import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import template from './comment.html';
import {
    Comments
}
from '../../api/Comment.js';
import {
    Resources
}
from '../../api/Resource.js';

function CommentCtrl($scope) {
    this.$onInit = function () {
        //$scope.addingComment = false;
        $scope.showingComment = false;
        $scope.showingNewCommentForm = false;
        $scope.myResourceId = $scope.$parent.myResourceId
        console.log("my resourceId:  " + $scope.$parent.myResourceId);
        //console.log($scope.$parent.myResourceId)
        //$scope.myResourceId = $scope.$parent.resource._id;
        $scope.userName = JSON.parse(localStorage.getItem("currentUser")).name;   
        $scope.newComment = {
            userId: JSON.parse(localStorage.getItem("currentUser"))._id
        };
        // console.log($scope.newComment);
        $scope.helpers({
            comments() {
                return Comments.find({
                    resourceId: $scope.myResourceId
                })
            }
        });
    }
    $scope.showComments = function(){
        $scope.showingComments = true;
    }
    $scope.showNewCommentForm = function(){
        $scope.showingNewCommentForm = true;
    }
    $scope.addComment = function () {
        
        Comments.insert({
            userId: $scope.newComment.userId
            , resourceId: $scope.myResourceId
            , comment: $scope.newComment.comment,
            userName: $scope.userName
        });
        //hide all the things
        //$scope.addingComment = false;
        $scope.showingNewCommentForm = false;
        $scope.newComment = {
            userId: localStorage.getItem("currentUser")._id
        };
    }
}
export default angular.module('comment', [angularMeteor, uiRouter]).component('comment', {
    templateUrl: 'imports/components/comment/comment.html'
    , controller: CommentCtrl
});