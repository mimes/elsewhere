import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import {
    Mongo
}
from 'meteor/mongo';
import {
    Users
}
from '../../api/User.js';
import template from './login.html';

function LoginCtrl($scope) {
    console.log(Users);
    $.getScript("https://apis.google.com/js/platform.js", function (data, textResult, jqXHR) {
        //console.log(textResult);
        console.log(gapi);
        //console.log(window.gapi);
    });

    function onSignIn(googleUser) {
        //callback for google sign in. see: https://developers.google.com/identity/sign-in/web/sign-in
        var profile = googleUser.getBasicProfile();
        try {
            $scope.user = googleUser;
            //console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            //console.log('Name: ' + profile.getName());
            //console.log('Image URL: ' + profile.getImageUrl());
            //console.log('Email: ' + profile.getEmail());
            var currentUser = {
                "email": profile.getEmail()
                , "name": profile.getName()
            };
            //toggle the below two lines to hanlde redirect in development or production environment
            window.location.href = "http://localhost:3000/home"; //< development option
            //window.location.href = "http://elsewhere.meteorapp.com/home" //< production option
            Users.insert({
                email: profile.getEmail()
                , name: profile.getName()
            });
            currentUser.id = Users.findOne({
                email: profile.getEmail()
            })._id;
            localStorage.setItem("currentUser", JSON.stringify(currentUser));
            //alert(currentUser.id)
        }
        catch (error) {
            throw ("signin error:  " + error);
        }
    }
    //sends the sign in  function to global scope so that google's code will hook properly. won't work from controller scope
    window.onSignIn = onSignIn;
    
}
export default angular.module('login', [angularMeteor, uiRouter]).component('login', {
    templateUrl: 'imports/components/login/login.html'
    , controller: LoginCtrl
}).config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider.state('login', {
        url: '/login'
        , template: '<login></login>'
    });
}