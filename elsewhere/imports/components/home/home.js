import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import template from './home.html';
import {
    Classes
}
from '../../api/Class.js';

function HomeCtrl($scope) {
    this.$onInit = function () {
        $scope.myUserId = JSON.parse(localStorage.getItem("currentUser")).id;
        
        $scope.searchingForClass = false;
        $scope.creatingClass = false;
    }
    $scope.helpers({
        classes() {
            return Classes.find({}).fetch();
        }
    });
    $scope.addClass = function () {
        //insert mongodb record
        Classes.insert({
            classId: $scope.newClass.className
            , className: $scope.newClass.className
            , description: $scope.newClass.description
            , userId: $scope.myUserId
        });
        //clear new class
        $scope.newClass = {};
    };
    $scope.filters = {
        search: ''
    };
    $scope.user = JSON.parse(localStorage.getItem("currentUser"));
    console.log($scope.user);
    console.log($scope.user.name);
    console.log($scope.user.email);
    //models the new class as it is created before it's inserted
    /*    $scope.showClassCreateForm = function(){
            
        }*/
}
export default angular.module('home', [angularMeteor, uiRouter]).component('home', {
    templateUrl: 'imports/components/home/home.html'
    , controller: HomeCtrl
}).config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider.state('home', {
        url: '/home'
        , template: '<home></home>'
    });
}