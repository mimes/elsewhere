import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import template from './classItem.html';
import {
    Classes
}
from '../../api/Class.js';
import {
    SubscribedClasses
}
from '../../api/SubscribedClass.js';
import {
    Topics
}
from '../../api/Topic.js';
import {
    Resources
}
from '../../api/Resource.js';

function ClassCtrl($scope, $stateParams) {
    this.$onInit = function () {
        var FLAGGED_ITEM_ENUM_TYPE_CLASS = 0;
        $scope.itemId = $stateParams.id
        $scope.itemType = FLAGGED_ITEM_ENUM_TYPE_CLASS;
        $scope.user = JSON.parse(localStorage.getItem("currentUser"));
        $scope.addingTopic = false;
        $scope.newTopic = {};
    }
    $scope.helpers({
        classItem() {
            return Classes.findOne({
                _id: ($stateParams.id)
            });
        }, topics() {
            return Topics.find({
                classId: $stateParams.id
            });
        }
    });
    //set the content creator id so that child controls have access to it
    var thisClass = Classes.findOne({
        _id: ($stateParams.id)
    });

    if (thisClass) {
        $scope.contentCreatorId = thisClass.userId
    }else{
        $scope.contentCreatorId = "error"
    }

//
$scope.subscribeToClass = function () {
        SubscribedClasses.insert({
            className: $scope.classItem.className
            , email: $scope.user.email
            , classId: $scope.classItem._id
        })
    }
    //
$scope.addTopic = function () {
    Topics.insert({
            classId: $scope.classItem._id
            , topicName: $scope.newTopic.topicName
            , description: $scope.newTopic.description,
            userId:  $scope.user.id
        })
        //reset Topic var
    $scope.newTopic = {};
    $scope.addingTopic = false;
}
$scope.$on("collapseOtherTopics", function (e, topicIdToExclude) {
    $scope.$broadcast("collapseTopic", topicIdToExclude);
})
}
export default angular.module('class', [angularMeteor, uiRouter]).component('class', {
    templateUrl: 'imports/components/classItem/classItem.html'
    , controller: ClassCtrl
});