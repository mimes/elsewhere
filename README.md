# ELSEWHERE (prototype)#

The code hosted in this repo is a prototype for a community-based resource sharing application.  The prototype was created using MeteorJS as the platform to speed up the development process.

### Technologies Used ###

* angularJS 1.5x for front end framework (modified to work with MeteorJS)
* mongoDB for DBMS
* API abstracted away via MeteorJS
* Note:  Trying to do unit/integration testing with meteor and angularJS at the same time drove me away from taking this version out of prototype phase. 

### Organization of Code ###

The directory structure for this repo is an almgamation of what's reasonable for an angularJS app and what's reasonable/necessary for a MeteorJS app:

imports/
  components/       	#contains views and their logic. e.g. components/classItem has the template and controller for a class item           
 
  api/					#the mongoDB schemas stored here are magically given an API via meteor. Magical.
client/
  main.js               # client entry point, imports all client code
server/
  main.js               # server entry point, imports all server code

### What next?###

I am working on a non-prototype version of the application as I am able while in school/on co-op.  I'm watching front-end technologies closely as they continue to rapidly change while working on the developing the API (and testing it, in a much less painful way :) ).   Right now, I'm leaning towards front-end technologies that have web components at their center.